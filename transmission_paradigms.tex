Standard TCP uses the \gls{cwnd} concept to maintain the count of
how many segments can stay in flight between the end hosts at a given moment
(called the \emph{window-based} transmission paradigm). Therefore, the sender can
transmit data that ranges from the first byte that is not yet acknowledged
(SND.UNA) to the amount indicated by the \gls{cwnd} (or the receiver window, if the
resources of the receiver limit the sender). When new data is acknowledged, the
window moves forward: this is the sliding mechanism behind any TCP, and this is
why the TCP protocol is ACK-clocked.

\medskip{\textbf{Understanding the burst-based paradigm.}}
In the \emph{window-based} paradigm, as we said before, the sender can send new data
when the congestion window increases due to the reception of an ACK or a
duplicated ACK, or when data is acknowledged (implying the reception of a
non-duplicated ACK). The \emph{burst-based} transmission avoids this syncing with the
reception of ACKs: the algorithm increases at regular times the congestion
window by a number of segments equal to the burst size. The effect on the wire
is the presence of bursts of segments separated by a certain amount of time. In
the following, the burst size is indicated by the variable
\emph{BURST}, while \emph{TxTime} indicates the time between two bursts. As a side effect
of the burst transmission, there is the generation of ACK trains. These trains
have a certain RTT, which is a good meter of the current congestion, and a
particular ACK train dispersion which tells about the bottleneck available
capacity~\cite{kleinrock}. Decoupling the ACK reception process from the transmission
requires a careful tuning to avoid adverse effects such as congestion. For this
reason, it is necessary to monitor the ACK flows and proactively estimate a set of
related statistics/information about the channel congestion status and
available capacity, to update both \emph{BURST} and \emph{TxTime} accordingly.

\medskip{\textbf{Understanding the rate-based paradigm.}}
Since the release of Linux 4.13, there is another mechanism for sending out
data without waiting for the reception of one ACK, and it is called pacing. We
can call this paradigm \emph{rate-based}, to differentiate it from the \emph{window-} and
\emph{burst-based} ones. In the \emph{window-based} paradigm, when the congestion window suddenly
increases, there could be a burst of segment injected into the network. The
pacing is a direct countermeasure: it spaces these segments by an amount of
time calculated from a given input rate. Modern algorithm, such as
BBR, calculates this rate autonomously, while for older algorithms the TCP socket
automatically calculates it as $\frac{cwnd * mss}{smoothed RTT}$. During the
congestion avoidance phase is used the 120\% of the previous result, while in
the slow start phase is used the 200\%. For example, if the congestion control
increases the congestion window by 20 segments after the reception of a
cumulative ACK, and the same congestion control set a rate of 100 segments per
seconds, shortly after the reception of the ACK above only one segment is
allowed to leave the TCP level. The others 19 have to wait 10 ms each, due to
the pacing rules. Consequently, the pacing mechanism is spreading the
congestion window increment over 200 ms, reaching its objective, and
incidentally decoupling (except for the first segment of a window increase) the
transmission with the ACK reception.

The strategy that we chose to implement TCP Wave on Linux is to re-utilize the
mechanisms and the calls that are currently present in the kernel. In doing
this, we ended up in slightly modifying the \emph{rate-based} mechanism: the objective
was to adapt it to support a \emph{burst-based} paradigm. We will divide these general
edits, which inserted four new functions in the interface between the TCP
socket and a congestion algorithm from how the TCP Wave module implements them,
and its operating principles. It is worth to note that these functions can also
be used by other (future) CCAs, without impacting the existing ones.

