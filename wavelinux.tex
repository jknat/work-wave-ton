% For Grammarly
We start the description of TCP Wave inside the Linux kernel by first
describing the modifications done at the interface level. The interface between
a TCP socket and a congestion control algorithm is well defined, and as a
matter of fact, the developers are historically writing congestion control (and
congestion avoidance) algorithms as
kernel modules. TCP Wave is not an exception, but we had to introduce some
(small) changes inside the internals of the TCP socket to implement the
burst-based transmission. In particular, we tweaked the pacing mechanism, to
allow a congestion control to shape it. A possible form of this mechanism can
re-create the burst-based paradigm that we have seen before. Before defining
how we tweaked pacing mechanism, it is necessary to explain how the kernel
developers implemented it in the current upstream version (4.13). Instead of
reporting the current code, we have abstracted it, and we present it in the
form of pseudocode in Figure~\ref{alg:PacingLinuxUpstream}.

\medskip{\textbf{Upstream Pacing implementation.}}
The pacing mechanism main component is a timer, which has an expiration time
and an associated function to invoke. We will refer to it as \emph{pacing\_timer}, and
we assume that a \gls{caa} signals that it needs the pacing mechanism (the function
\fun{NeedsPacing} returns true). The TCP socket tries to send out as much data as
possible in the function \fun{SendData} (which is a pseudocode version of the kernel
function \emph{tcp\_write\_xmit}) through the use of a loop, which is interrupted when
it is not possible to send other pieces out (e.g., because of window
restrictions). One of the reasons for breaking the loop is to the pacing timer:
if it is active, then the loop is exited, without sending data. If it is not
active, others check are performed (such as on the congestion window, on the
receiver window, and so on) and if all are positive, then it is possible to
send (which is, in reality, an enqueue into the lower layer queueing
discipline) a single segment. Just before the enqueue operation, the timer is
activated and set to expire in the future, considering the length of the
segment to send (which can be larger than the MTU, because of various
techniques of offloading) and the current rate, returned by the \gls{cca} or
calculated automatically. When the pacing timer expires (which is uncorrelated
to the reception of an ACK) it calls the function \fun{SendData}, closing the circle
and trying to send out more data.

\medskip{\textbf{Our modifications to the Pacing implementation.}}
If we have a look at Figure~\ref{alg:PacingLinuxWave}, we can see the
modifications that we inserted in this mechanism to ensure a burst-based
transmission. The function \fun{TcpInternalPacing} (that resembles the kernel
function \emph{tcp\_internal\_pacing}) has a check on the pacing timer: if it is
already running, the code exits without setting another expiration time. Then,
if the congestion control implements a specific function to calculate the
pacing timer (\fun{GetPacingTime} in pseudocode) this specific function is used to
retrieve the value in nanoseconds of the pacing timer, instead of calculating
it from the rate and the packet size. The second modification is inside the
function \fun{SendData}. Before entering the loop for transmitting data, a variable
\emph{max\_segs} is initialized to 0. It represents the maximum number of segments
that can be sent out if the pacing timer is off and the socket needs pacing.
So, assuming that the socket needs pacing, first of all, \fun{SendData} checks if the
\gls{caa} implements a function named \fun{PacingExpired}. If yes, it calls it. The next
step is to check if the \gls{cca} implements a function named \fun{GetSegs}. In the
affirmative case, the function retrieves the maximum number of segments that
can be sent out before setting the pacing timer, into the variable \emph{max\_segs}.
If the CCA does not implement the function, the default value of 1 is used
(therefore, the result is that only one segment can be sent out). After
starting the data transmission loop, the pacing restraint now checks that the
number of the segments that we sent is lower than the valued stored in
\emph{max\_segs} to continue sending data. These modifications are backward-compatible
because a CCA that does not implement the newer function has the same behavior
of the upstream version (one segment before setting the pacing timer, and the
pacing time calculated as before) while newer CCA can tune these values. For
instance, for achieving a burst-based transmission it is necessary to implement
the function \fun{GetPacingTime}, returning the time between the burst, and then the
function \fun{GetSegs}, returning the number of the segments of which the burst is
composed. Then, it is necessary to make the proper congestion window adjustment
to make room for the outgoing burst, role which is taken by the function
\fun{PacingExpired}.

TCP Wave implementation of these two function is straightforward: as pacing
time, it returns the value of the transmission timer (\emph{TxTime}), while as the number of
segments it returns the burst size (\emph{BURST}). TCP Wave uses the third method that we
introduced, \fun{PacingExpired}, to increase the congestion window size of the number
of segments indicated as burst size.  The idea behind it is that each time the
timer expires, it triggers a congestion window adjustment to make space for the
new burst to send.

\medskip{\textbf{Monitoring the ACK flows.}}
When we designed TCP Wave, we tried to keep the burst size as fixed as
possible, initialized at the loading time of the module. The pacing timer
changes accordingly to the network conditions, and therefore the TCP Wave
operational scheme starts with a proper measurement of the incoming ACK
samples. At this scope, we implemented the function \emph{cong\_control}, which is an
interface function to be implemented by the CCA, that aims to replace the rigid
increase and decrease of the congestion window. The TCP socket calls it toward
the end of processing an ACK with precise rate information, such as RTT, the
number of delivered packets, and many others. All transmission or
retransmission are delayed afterward, meaning that it is safe to calculate all
the transmission parameters inside this function. An ACK segment carries the
information (the ACKed sequence number) that can refer to a segment that was
transmitted at the beginning, the middle, or at the end of a burst, and
different actions are performed in each of these cases. To correctly identify
the position of the segment that generated the ACK, the code saves the burst
sizes inside a dedicated queue and keeps track of the number of ACKs received.
For the sake of simplicity, let us assume that a single burst of ten segments
has been sent out in the past, and now the TCP Wave \emph{cong\_control} function is
invoked. First of all, the RTT is compared to the minimum and the maximum
values seen and saved as the new minimum or maximum if it is the case. In our
simple explanation, we are receiving the first ACK of the train, so the time is
saved to a dedicated variable: subsequent ACK reception will find this variable
initialized, therefore marking the fact that the segments that generated these
ACKs were not at the beginning of a burst. Then, until the variable that count
the incoming ACKs is equal to the oldest burst size we have in memory, the next
ACKs are all generated by segments in the middle of a burst. For all these
segments, we update the last reception time that will later be used to
calculate the ACK train dispersion (the ACK train dispersion is calculated as
last reception time minus first reception time). When the counting variable
finally equals the burst size, we reached the end of a burst: therefore, it is
possible to calculate the ACK train dispersion. The time of the reception of
the first ACK is reset and therefore is possible at the next ACK reception it
is possible to restart the algorithm.

\medskip{\textbf{Error mitigation.}}
In two cases the above picture does not give the correct result: when the
delayed ACK mechanism is triggered precisely for the last segment of the burst,
and when a cumulative ACK that covers more than one burst of data is received.
In the first case, the consequence is that the ACK covers the bytes of the
latest segment of the burst and the first of the following burst. We chose to
calculate the ACK train dispersion using the previously saved value for the
last reception time (which refers to a segment in the middle of the burst) and
to set the current reception time as the beginning of the next round. The
second case can be divided into two subcases. The first thing to notice is that
it is impossible to calculate a new value for the ACK train dispersion, as the
ACK is only one. Therefore, if we already have a real sample for the ACK train
dispersion, we will use it. Otherwise, we employ a heuristic: as ACK train
dispersion we use the delivery time calculated by the TCP socket, divided by a
factor which is increased each time we have to use the heuristic. In case the
code is forced to use the heuristic continuously (e.g., a middlebox performs
ACK aggregation) after some time the protection mechanism of TCP Wave
(Adjustment Mode) will trigger, therefore trying to compensate the congestion
introduced by the heuristic.

\begin{figure}
\begin{mdframed}
    \caption{Pseudocode of the pacing mechanism in Linux 4.13}
    \label{alg:PacingLinuxUpstream}
    \begin{algorithmic}
      \Procedure{TcpInternalPacing}{socket *sk, segment *skb}
        \State len\_ns $\gets$ \Call{SizeOf}{skb} * NSEC\_PER\_SEC
        \State rate $\gets$ \Call{GetCurrentRate}{sk}
        \State len\_ns $\gets$ $\frac{len\_ns}{rate}$
        \State \Call{StartTimer}{len\_ns}
      \EndProcedure

      \State

      \Procedure{TransmitSingleSegment}{socket *sk, segment *skb}
        \State \Call{PrepareHeader}{skb}
        \If{\Call{NeedsPacing}{sk}}
          \State  \Comment returns true if pacing for sk is needed
          \State \Call{TcpInternalPacing}{sk, skb}
        \EndIf
        \State \Call{QueueXmit}{sk, skb}
      \EndProcedure

      \State

      \Procedure{TcpPacingCheck}{sk}
        \State a $\gets$ \Call{NeedsPacing}{sk}
        \State b $\gets$ \Call{IsTimerActive}{sk}
        \State  \Comment b is true if pacing\_timer for sk is active
        \State \Return a $\wedge$ b;
      \EndProcedure

      \State

      \Procedure{SendData}{socket *sk}
        \While{sk has segments}
          \If{TcpPacingCheck(sk)}
            \State ExitLoop
          \EndIf
          \State ...
          \State segment *skb $\gets$ GetHead(sk)
          \State \Call{TransmitSingleSegment}{sk, skb}
        \EndWhile
      \EndProcedure
    \end{algorithmic}
\end{mdframed}
\end{figure}

\begin{figure}
\begin{mdframed}
    \caption{Pseudocode of the pacing mechanism for burst-based transmissions}
    \label{alg:PacingLinuxWave}
    \begin{algorithmic}
      \Procedure{TcpInternalPacing}{sk, skb}
      \If{\Call{NeedsPacing}{sk}}
          \State \Return
        \EndIf
        \If{CCA\_GetPacingTime}
        \State len\_ns $\gets$ \Call{CCA\_GetPacingTime}{sk}
        \Else
          \State len\_ns $\gets$ \Call{SizeOf}{skb} * NSEC\_PER\_SEC
          \State rate $\gets$ \Call{GetCurrentRate}{sk}
          \State len\_ns $\gets$ $\frac{len\_ns}{rate}$
        \EndIf

        \State \Call{StartTimer}{len\_ns}
      \EndProcedure

      \State

      \Procedure{SendData}{socket *sk}
        \State max\_segs $\gets$ 0
        \If{\Call{NeedsPacing}{sk} $\wedge \not$ \Call{IsTimerActive}{sk}}
          \If{CCA\_PacingExpired}
            \State \Call{CCA\_PacingExpired}{sk}
          \EndIf
          \If{CCA\_GetSegs}
            \State max\_segs $\gets$ \Call{CCA\_GetSegs}{sk}
          \Else
            \State max\_segs $\gets$ 1
          \EndIf
        \EndIf

        \While{sk has segments}
          \If{\Call{NeedsPacing}{sk} $\wedge$ max\_segs $\leq$ sent}
            \State ExitLoop
          \EndIf
          \State ...
          \State segment *skb $\gets$ GetHead(sk)
          \State \Call{TransmitSingleSegment}{sk, skb}
        \EndWhile
      \EndProcedure
    \end{algorithmic}
\end{mdframed}
\end{figure}
