
The Linux networking subsystem is complicated. The complexity derives mainly
from two reasons: the first is that the coexistence of different Internet
protocols features is not always straightforward. The \gls{ietf}
standardizes protocols, and their optional features, in documents
called \gls{rfc}. Currently, there are thousands of \glspl{rfc}, of
which dozens are related to TCP. Often, different specifications use a
partially different set of concepts, and to include them at the same time (as
we would expect from a modern and widely used operating system), the
implementation has to be general and modular enough, with the downside that
following every single aspect in detail is intricated. The second complexity
source is the fact that is almost impossible to find a Linux-based system which
does not use any part of the networking subsystem. Therefore, the code must be
correct, but also fast: performance is a crucial factor in the modern era of
computing. The \gls{gcc} compiler can do many optimizations, but still, humans are
responsible for the high-level logic, the data structures management, the API,
and many other things that make a real difference. Just to have an idea, the
code should be fast enough to perform well over high-end network devices, at a
line rate (from 10 to 40 Gb per second, and who knows what will happen in one
or two years). To work with a standard interface at a nominal rate of 10 Gb/s,
and packets of 1500 bytes in length, each packet should traverse the stack in
1.2 microseconds to maintain the nominal rate.

Even if it is out of the scope of this paper to present in detail the
architecture of the Linux TCP implementation, we would like to point out the
main (sender-side) characteristics to make the paper self-contained. The
interested reader can refer to~\cite{linuxtcp}, which at the time of writing is the
most up-to-date paper that explains the architecture on which we are building
the TCP Wave module.

\medskip{\textbf{ACK processing.}} When an ACK arrives, the first thing is to
decide what to send next. Thanks to the ACK and SACK information, the module
can track the delivered segments and also if one or more segments were lost,
allowing for one or more retransmissions before sending out new data. Moreover,
it also tracks the timing information about the ACK-ed segments, to be able to
estimate the RTT and the delivery rate.

\medskip{\textbf{Congestion control.}} Then, we have the congestion control. It
has to decide the maximum amount of in-flight data giving the network
conditions, inferred by the ACK reception. After the introduction of the pacing
mechanism (which we will explain below), it has to decide also how fast to
enqueue that data in the lower layer. A \gls{cca} or \gls{caa} is a
pluggable component of the TCP socket, or in other words, the algorithm should
adhere to a predefined interface and must implement specific functions that
will be invoked by the TCP socket code in particular situations. The core
decisions are thus made entirely in a separate kernel module. Linux has many
modules, and they can be swapped at run-time, or even used in parallel to serve
different flows.

\medskip{\textbf{TSO and TSQ.}} Below the congestion control, there are two
tightly couples algorithm: the first is \gls{tso} 
auto-sizing. It selects the size of the segment that TCP hands off to lower
layers to transmit. As the reader can imagine, larger segments will be split by
lower layers into MTU-sized blocks (therefore injecting a possibly massive
burst of data in the network) but a lot of small segments will have a
substantial impact over the CPU efficiency of the stack (by spending more time
into constructing packet headers). The \gls{tso} auto-sizing algorithm tries to find
a balance between these two needs. Then, there is gls{tsq}
algorithm which performs a local flow control to reduce latency: it limits the
amount of data that can be enqueued in the queuing discipline, keeping the
queues small to reduce latency and head-of-line blocking.

\medskip{\textbf{Pacing.}} Almost at the end of the processing process, there
is the pacing mechanism. Before the 4.13 release, it was implemented as a
fundamental part of the FQ packet scheduler. After the 4.13 release another
pacing mechanism was introduced, this time directly at the TCP layer. The
objective of the TCP pacing algorithm is to enqueue segments, according to the
rate specified by the congestion control, into the queueing discipline in the
lower layer.

\medskip{\textbf{Packet scheduling.}} When a segment finally reaches the lower
layer, it begins the complicated world of the packet scheduling, with
class-less or class-full disciplines, and many different algorithms to choose.
Even directly going to the surface of these algorithms and implementations will
be totally out of the scope of this paper. We have to cite this layer for
completeness, but we remind the interested readers to~\cite{}.

\medskip{\textbf{Device Drivers.}} At the end of the network stack, we have the
device driver. It can be an ethernet driver, or another wired support, as well
as one of the many wireless supported hardware, ranging from WiFi to LTE
technologies. It is important to remember that these technologies can add
others layers of complexity to the stack. For instance, both LTE and WiFi have
their own QoS classifiers, and their internal layers. The interaction between
them and the upper part of the stack is currently an ongoing research topic.

\begin{figure}
\centering
\caption{Linux TCP sender architecture}
\label{fig:tcp-class-diagram}

\begin{tikzpicture}[remember picture,node distance=.8cm,start chain= 1 going below]
  \node[punktchain, draw=gray, very thick, inner sep=0em] (tcp) {
    \begin{tikzpicture}
      [node distance=.8cm,start chain= 2 going below]
       \node[color=gray] (nametcp) {TCP};
       \node[punktchain, draw=black, very thick, below= -0.5em of nametcp] (ackprocessing) {ACK processing \\(what to send next)};
       \node[punktchain, draw=black, very thick, join] (congcontrol)   {Congestion control \\(how fast to send)};
       \node[punktchain, draw=black, very thick, join] (tso)      {TSO \\(how large a segment)};
       \node[punktchain, draw=black, very thick, join] (tsq) {TSQ \\(how much to enqueue)};
       \node[punktchain, draw=black, very thick, join] (pacing) {Pacing \\(when to send)};
    \end{tikzpicture}
  };
  \node[punktchain, draw=gray, very thick, join, inner sep=0em] (QDisc) {
    \begin{tikzpicture}
        \node[color=gray] (nameqdisc) {QDisc};
        \node[stylish, draw=black, very thick, below= -0.5em of nameqdisc] (sched) {Scheduler \\(how to mix)};
    \end{tikzpicture}
  };
  \node[punktchain, draw=gray, very thick, join, inner sep=0em] (DD) {
    \begin{tikzpicture}
      \node[color=gray] (namedd) {Device Driver};
      \node[stylish, draw=black, very thick, below= -0.5em of namedd] (niccontainer) {NIC};
    \end{tikzpicture}
  };
\end{tikzpicture}
\end{figure}
